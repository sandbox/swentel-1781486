<?php

/**
 * @file
 * Drush functions for Field API.
 */

/**
 * Implements hook_drush_command().
 */
function drush_fieldapi_drush_command() {
  $items = array();

  $items['fieldapi-ctype'] = array(
    'aliases' => array('fact'),
    'description' => 'Generate a content type with fields and instances',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['fieldapi-ab'] = array(
    'aliases' => array('faab'),
    'description' => 'Generate ab url\'s for a content type',
    'arguments' => array(
      'node_type',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Drush callback: generate ab commands.
 */
function drush_drush_fieldapi_fieldapi_ab($node_type = '', $base_url = '', $number = 10) {

  if (empty($node_type)) {
    return drush_user_abort(dt('Make sure you provide a node type.'));
  }

  if (empty($base_url)) {
    $base_url = 'drupal8';
    drush_log('Add an additional argument to construct your base url (defaults to "drupal8")', 'success');
  }

  drush_log('You need to inspect the cookies in your browser to add the cookie name and value so you can measure authenticated requests. Add "-C sid:value as an argument to the ab command."', 'success');

  $nodes = db_query('SELECT nid FROM {node} WHERE type = :type LIMIT ' . $number, array(':type' => $node_type));
  foreach ($nodes as $node) {
    drush_log(dt("ab -n 300 http://@base/node/@nid", array('@base' => $base_url, '@nid' => $node->nid)), 'success');
    drush_log(dt("ab -n 300 http://@base/node/@nid/edit", array('@base' => $base_url, '@nid' => $node->nid)), 'success');
  }
}


/**
 * Drush callback: generate content type with fields and instances.
 *
 * Happily based on Simpletest.
 */
function drush_drush_fieldapi_fieldapi_ctype($text_fields = 10, $taxonomy_fields = NULL, $cardinality = 1, $nr_ctypes = 1) {

  while ($nr_ctypes > 0) {
    // Find a non-existent random type name.
    do {
      $name = strtolower(_drush_fieldapi_random_name(8));
    } while (DRUPAL_CORE_COMPATIBILITY == '7.x' ? node_type_get_type($name) : node_type_load($name));

    // Populate defaults array.
    $defaults = array(
      'type' => $name,
      'name' => $name,
      'base' => 'node_content',
      'description' => '',
      'help' => '',
      'title_label' => 'Title',
      'body_label' => 'Body',
      'has_title' => 1,
      'has_body' => 1,
    );
    // Imposed values for a custom type.
    $forced = array(
      'orig_type' => '',
      'old_type' => '',
      'module' => 'node',
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    );
    $type = $forced + $defaults;
    $type = (object) $type;

    $saved_type = node_type_save($type);
    node_types_rebuild();
    DRUPAL_CORE_COMPATIBILITY == '7.x' ? menu_rebuild() : menu_router_rebuild();
    node_add_body_field($type);

    drush_fieldapi_generate_textfields($name, $text_fields, $cardinality);

    if ($taxonomy_fields) {
      drush_fieldapi_generate_term_fields($name, $taxonomy_fields, $cardinality);
    }

    drush_log(dt('Content type "@name" generated.', array('@name' => $name)), 'success');
    drush_log(dt('Use following drush command to generate nodes of this type'), 'success');
    drush_log(dt('drush genc 10 --types=@type', array('@type' => $name)), 'success');
    drush_log(dt('Use following drush command to get ab commands for this node type after generating nodes.'), 'success');
    drush_log(dt('drush faab @type', array('@type' => $name)), 'success');

    $nr_ctypes--;
  }
}

/**
 * Generate textfields.
 */
function drush_fieldapi_generate_textfields($bundle, $number, $cardinality) {
  while ($number != 0) {
    $field = _drush_field_api_generate_textfield_field($bundle, $cardinality);
    $instance = _drush_field_api_generate_textfield_instance($field, $bundle);
    field_create_field($field);
    $display = $instance['display'];
    unset($instance['display']);
    field_create_instance($instance);
    foreach ($display as $view_mode => $settings) {
        entity_get_display('node', $bundle, $view_mode)
      ->setComponent($field['field_name'], $settings)
      ->save();
    }
    $number--;
  }
}

/**
 * Generate taxonomy term fields.
 */
function drush_fieldapi_generate_term_fields($bundle, $number, $cardinality) {
  while ($number != 0) {
    $field = _drush_field_api_generate_term_field($bundle, $cardinality);
    $instance = _drush_field_api_generate_term_instance($field, $bundle);
    field_create_field($field);
    $display = $instance['display'];
    unset($instance['display']);
    field_create_instance($instance);
    foreach ($display as $view_mode => $settings) {
        entity_get_display('node', $bundle, $view_mode)
      ->setComponent($field['field_name'], $settings)
      ->save();
    }
    $number--;
  }
}

/**
 * Helper function to generate random name, happily copied from Simpletest.
 */
function _drush_fieldapi_random_name($length = 8) {
  $values = array_merge(range(65, 90), range(97, 122), range(48, 57));
  $max = count($values) - 1;
  $str = chr(mt_rand(97, 122));
  for ($i = 1; $i < $length; $i++) {
    $str .= chr($values[mt_rand(0, $max)]);
  }
  return $str;
}

/**
 * Helper function to generate a textfield field.
 */
function _drush_field_api_generate_textfield_field($bundle, $cardinality) {

  static $number = 0;

  return array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(
      'max_length' => '255',
    ),
    'foreign keys' => array(
      'format' => array(
        'table' => 'filter_format',
        'columns' => array(
          'format' => 'format',
        ),
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'field_name' => 'field_text_' . $bundle . '_' . $number++,
    'type' => 'text',
    'module' => 'text',
    'active' => '1',
    'locked' => '0',
    'cardinality' => $cardinality,
    'deleted' => '0',
  );
}

/**
 * Helper function to generate a textfield instance.
 */
 function _drush_field_api_generate_textfield_instance($field, $bundle) {

  static $weight = 0;

  return array(
    'label' => ucfirst(strtr($field['field_name'], '_', ' ')),
    'widget' => array(
      'weight' => $weight++,
      'type' => 'text_textfield',
      'module' => 'text',
      'active' => 1,
      'settings' => array(
        'size' => '60',
      ),
    ),
    'settings' => array(
      'text_processing' => '0',
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'text_default',
        'weight' => '4',
        'settings' => array(),
        'module' => 'text',
      ),
    ),
    'required' => 0,
    'description' => '',
    'default_value' => NULL,
    'field_name' => $field['field_name'],
    'entity_type' => 'node',
    'bundle' => $bundle,
    'deleted' => '0',
  );
}

/**
 * Helper function to generate a term field.
 */
function _drush_field_api_generate_term_field($bundle, $cardinality) {

  static $number = 0;

  return array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'tags',
          'parent' => '0',
        ),
      ),
    ),
    'foreign keys' => array(
      'tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array(
          'tid' => 'tid',
        ),
      ),
    ),
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'field_name' => 'field_term_' . $bundle . '_' . $number++,
    'type' => 'taxonomy_term_reference',
    'module' => 'taxonomy',
    'active' => '1',
    'locked' => '0',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'deleted' => '0',
  );
}

/**
 * Helper function to generate a term instance.
 */
 function _drush_field_api_generate_term_instance($field, $bundle) {

  static $weight = 0;

  return array(
    'field_name' => $field['field_name'],
    'entity_type' => 'node',
    'label' => ucfirst(strtr($field['field_name'], '_', ' ')),
    'bundle' => $bundle,
    'description' => '',
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -4,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
}


